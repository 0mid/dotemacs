(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C--") 'text-scale-decrease)

;; Bind Pause/Break key to "Type q" in other-window, effectively
;; providing a way for burying *Help* buffers and the like.
(global-set-key (kbd "<pause>") (kbd "C-x o q"))

(global-set-key (kbd "C-c d") (lambda () (interactive (delete-pair))))

;; backward kill line (equivalent to C-0 C-k)
(global-set-key (kbd "C-<backspace>") (lambda ()
					(interactive)
					(kill-line 0)))

;;Have M-j join the following line onto the current one.
(global-set-key (kbd "M-j") (lambda () (interactive) (join-line -1)))

;; ----------------------------------------------------------------------
;; Temporary line numbers
;;
;; This snippet shows line numbers temporarily just when you're going
;; to a line number with goto-line (M-g M-g). Notice the nice
;; remap-trick in the key binding. It will remap all key bindings from
;; goto-line to goto-line-with-feedback.
;;
;; http://whattheemacsd.com//key-bindings.el-01.html

(defun goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input"
  (interactive)
  (unwind-protect
      (progn
	(linum-mode 1)
	(goto-line (read-number "Goto line: ")))
    (linum-mode -1)))

(global-set-key [remap goto-line] 'goto-line-with-feedback)
;;
;; ----------------------------------------------------------------------

;; ----------------------------------------------------------------------
;; Copy current line with M-w and kill current line with C-w
;;
;; http://xahlee.org/emacs/emacs_copy_cut_current_line.html

(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy the current line."
  (interactive
   (if mark-active
       (list (region-beginning) (region-end))
     (progn
       (message "Current line is copied.")
       (list (line-beginning-position) (line-beginning-position 2)) ) ) ))

(defadvice kill-region (before slick-copy activate compile)
  "When called interactively with no active region, cut the current line."
  (interactive
   (if mark-active
       (list (region-beginning) (region-end))
     (progn
       (list (line-beginning-position) (line-beginning-position 2)) ) ) ))
;;
;; ----------------------------------------------------------------------

;; ----------------------------------------------------------------------
;; Push and pop mark
;; http://www.masteringemacs.org/articles/2010/12/22/fixing-mark-commands-transient-mark-mode/

(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region
Equivalent to \\[set-mark-command] when \\[transient-mark-mode]
is disabled"
  (interactive)
  (push-mark (point) t nil)
  (message "Pushed mark to ring"))

(defun jump-to-mark ()
  "Jumps to the local mark, respecting the `mark-ring' order.
This is the same as using \\[set-mark-command] with the prefix
argument."
  (interactive)
  (set-mark-command 1))

(global-set-key (kbd "C-`") 'push-mark-no-activate)
(global-set-key (kbd "M-`") 'jump-to-mark)
;;
;; ----------------------------------------------------------------------

;; Add alternative keybindings for beginning and end of paragraph and
;; buffer movement/jump keys to remove the awkward Shift (S) from them.

;; * Fix C-tab, PgUp, PgDn, etc., special keys in Emacs run in Cygwin
;;   minTTY terminal/console emulator:
;;
;; Make sure the following keybindings is NOT set. If any is set, keys
;; are translated into wrong chars (e.g., =C-tab= would insert the
;; chars =9;5u= in the active buffer somewhere near the mark!).

;; (global-set-key (kbd "M-[") 'backward-paragraph)
;; (global-set-key (kbd "M-]") 'forward-paragraph)

(global-set-key (kbd "C-,") 'beginning-of-buffer)
(global-set-key (kbd "C-.") 'end-of-buffer)

(cua-selection-mode t)

;; Don't tabify after rectangle command
(setq cua-auto-tabify-rectangles nil)

;; Add alternative keybindings for page up (scroll down C-v) and page
;; down (scroll up M-v)
(global-set-key (kbd "M-n") 'cua-scroll-up)
(global-set-key (kbd "M-p") 'cua-scroll-down)

;; Add this to mode-hooks that override these keybindings, e.g.,
;; org-mode-hook.
(defun load-init-keybindings ()
  ;; * Fix C-tab, PgUp, PgDn, etc., special keys in Emacs run in Cygwin
  ;;   minTTY terminal/console emulator:
  ;;
  ;; Make sure the following keybindings is NOT set. If any is set, keys
  ;; are translated into wrong chars (e.g., =C-tab= would insert the
  ;; chars =9;5u= in the active buffer somewhere near the mark!).

  ;; (local-set-key (kbd "M-[")	'backward-paragraph)
  ;; (local-set-key (kbd "M-]")	'forward-paragraph)

  (local-set-key (kbd "M-n")	'cua-scroll-up)
  (local-set-key (kbd "M-p")	'cua-scroll-down)
  (local-set-key (kbd "C-,")	'beginning-of-buffer)
  (local-set-key (kbd "C-.")	'end-of-buffer)
  (local-set-key (kbd "C-c .")	'iedit-mode))

(defun comment-dwim-line (&optional arg)
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position)
				   (line-end-position))
    (comment-dwim arg)))

(global-set-key (kbd "C-;") 'comment-dwim-line)

(use-package unfill
  :ensure t
  :bind ([remap fill-paragraph] . unfill-toggle))


;; Fix Ctrl+Tab (C-tab) sent by mintty (3.6.1
;; (x86_64-pc-cygwin))---used by Cygwin, MinGW, etc.---to Emacs run in
;; an ssh session (within or without tmux), that is, for the case,

;; mintty> ssh server
;; server> emacs

;; A text terminal transmits only characters (more precisely, bytes),
;; not keys. Keys and keychords (keys with modifiers) are encoded as
;; character sequences. Keys that insert printable characters are sent
;; as themselves; function keys are sent as escape sequences. Most
;; escape sequences consist of the character ESC (?\e in Emacs
;; syntax) followed by two or more printable characters. See Control
;; and up/down keys in terminal for use by emacs and How do keyboard
;; input and text output work? for more information.

;; Emacs has a mechanism to translate escape sequences into its own
;; notion of keys. The translation table input-decode-map is
;; initialized when Emacs starts (or more precisely, when a new frame
;; is open: this variable has a different value on each terminal).
;; Sometimes Emacs doesn't know all the escape sequences sent by the
;; terminal.

;; In our case, it appears that Emacs has the wrong interpretation for
;; the escape sequence sent by Ctrl-Tab. You need to tell it to
;; interpret it as C-tab. First, figure out what the escape sequence
;; is. In a buffer, press Ctrl+Q then Ctrl+Tab. The command C-q causes
;; the next character, which is the escape character, to be inserted
;; literally, followed by the rest of the escape sequence.
;; (Alternatively, look at M-x view-lossage.) Then, define the desired
;; key mapping for this sequence, replacing the literal escape  with
;; \e (Emacs syntax) and dropping the closing ]. For example, [1;5n]
;; becomes "\e[1;5n" to be mapped.

;; In principle, input-decode-map should be set per terminal. In
;; practice, it's very rare to have two terminals in which the same
;; escape sequence encodes different keys, so a global setting will
;; work fine. [https://emacs.stackexchange.com/a/989,
;; https://unix.stackexchange.com/a/116630]
(define-key input-decode-map "\e[1;5I" (kbd "C-<tab>"))
(define-key input-decode-map "\e[1;5l" (kbd "C-,"))
(define-key input-decode-map "\e[1;5n" (kbd "C-."))

(provide 'init-keybindings)
