(use-package cc-mode
  :hook (c-mode-common . setup-cc-mode)
  :config
  (defun setup-cc-mode ()
    (c-toggle-comment-style -1)
    (load-init-keybindings))
  (setq c-basic-offset 2
        c-default-style "stroustrup"))

(use-package opencl-c-mode
  :ensure t
  :mode ("\\.cl$" "\\.clinc$"))

(use-package modern-cpp-font-lock
  :ensure t
  :config
  (modern-c++-font-lock-global-mode t))

;; Compiler output viewer
(use-package rmsbolt
  :ensure t)

(use-package nasm-mode
  :ensure t
  :defer t
  :mode ("\\.nasm$" "\\.asm$" "\\.s$")
  :config
  (setq indent-tabs-mode t))

(use-package clang-format
  :ensure t
  :config
  (defun clang-format-region-mozilla (s e)
    (interactive
     (if (use-region-p)
         (list (region-beginning) (region-end))
       (list (point) (point))))
    (clang-format-region s e "Mozilla"))

  (defun clang-format-region-google (s e)
    (interactive
     (if (use-region-p)
         (list (region-beginning) (region-end))
       (list (point) (point))))
    (clang-format-region s e "Google"))

  (defun clang-format-region-llvm (s e)
    (interactive
     (if (use-region-p)
         (list (region-beginning) (region-end))
       (list (point) (point))))
    (clang-format-region s e "LLVM")))

(use-package cmake-mode
  :ensure t)

(provide 'init-c)
