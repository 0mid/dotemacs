(use-package diff-mode
  :hook (diff-mode . read-only-mode)
  :config
  ;; Default to unified diffs
  (setq diff-switches "-u")

  (set-face-foreground 'diff-added "green4")
  (set-face-foreground 'diff-removed "red3")

  ;; Move to the coloured text you're interested in, and type C-u C-x =
  ;; (C-u M-x what-cursor-position) to view details of the character at
  ;; point, including any faces, in *Help* buffer.
  (set-face-attribute 'diff-refine-added nil :background "orange")
  (set-face-attribute 'diff-refine-removed nil :background "darkgray"))

(use-package ediff
  :ensure t
  :config
  (defun ediff-copy-both-to-C ()
    (interactive)
    (ediff-copy-diff ediff-current-difference nil 'C nil
                     (concat
                      (ediff-get-region-contents ediff-current-difference
                                                 'A
						 ediff-control-buffer)
                      (ediff-get-region-contents ediff-current-difference
                                                 'B
						 ediff-control-buffer))))
  (defun add-C-to-ediff-mode-map () (define-key ediff-mode-map "C"
				      'ediff-copy-both-to-C))
  (add-hook 'ediff-keymap-setup-hook 'add-C-to-ediff-mode-map)

  ;; Don't use the weird setup with the control panel in a separate frame.
  (csetq ediff-window-setup-function 'ediff-setup-windows-plain)

  ;; Ignore white space (Be careful when ediffing Python, though)
  (csetq ediff-diff-options "-w")

  ;; Restore the windows after Ediff quits
  (add-hook 'ediff-after-quit-hook-internal 'winner-undo))

(provide 'init-diff)
