;; mail mode for Mutt, It's All Text, or Thunderbird
(add-to-list 'auto-mode-alist '(".*mutt.*\\|.*itsalltext.*\\|.*eml.*"
				. mail-mode))

(add-hook 'mail-mode-hook 'turn-on-auto-fill)
(add-hook 'mail-mode-hook 'flyspell-prog-mode)

(add-hook
 'mail-mode-hook
 (lambda ()
   (define-key mail-mode-map [(control c) (control c)]
     (lambda ()
       (interactive)
       (save-buffer)
       (server-edit)))))

(defface message-single-quoted-text-face
  '((((class color)
      (background light))
     (:foreground "Sienna"))
    (((class color)
      (background dark))
     (:foreground "Chocolate"))
    )
  "Face used for displaying text which has been quoted (e.g. \">foo\")."
  )

(defface message-multiply-quoted-text-face
  '((((class color)
      (background light))
     (:foreground "Firebrick"))
    (((class color)
      (background dark))
     (:foreground "DarkGreen"))
    )
  "Face used for text which has been quoted more than once (e.g. \">>foo\")."
  )

(add-hook 'mail-mode-hook
	  (lambda ()
	    (font-lock-add-keywords
	     nil
	     '(("^[ \t]*>[ \t]*>.*$"
		(0 'message-multiply-quoted-text-face))
	       ("^[ \t]*>.*$"
		(0 'message-single-quoted-text-face))))))

(provide 'init-mail)
