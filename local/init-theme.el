(use-package solarized-theme
  :ensure t
  :config
  (load-theme 'solarized-light t))

(set-cursor-color "brown")

;; ansi-color translates ANSI SGR (Select Graphic Rendition) escape
;; sequences like "Esc [ 30 m" into EmacsOverlays, TextProperties, or
;; XEmacsExtents with face colours, bold, etc.
(use-package ansi-color
  :if (not (display-graphic-p))
  :ensure t)


(provide 'init-theme)
