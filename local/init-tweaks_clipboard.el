(setq mouse-drag-copy-region t
      mouse-yank-at-point t)

;; Work around Emacs hanging upon paste from X
;;
;; http://debbugs.gnu.org/cgi/bugreport.cgi?bug=16737#17
;; http://ergoemacs.org/misc/emacs_bug_cant_paste_2015.html
(setq x-selection-timeout 30)           ; milliseconds

(provide 'init-tweaks_clipboard)
