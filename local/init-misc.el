(use-package ace-window
  :ensure t
  :bind ([C-tab] . ace-window)
  :config
  (setq aw-scope 'frame))    ; default is global (works across frames)

(use-package re-builder
  :config
  (setq reb-re-syntax 'string))

(use-package uniquify
  :config
  (setq uniquify-buffer-name-style 'forward))

(use-package undo-tree
  ;; TODO: find out what's wrong with undo-tree's GPG signature that
  ;; fails check upon install attempt.
  :ensure nil
  :diminish ""
  :bind (:map undo-tree-map
              ("C-c C-/"   . undo-tree-undo)
              ("C-x u"     . undo-tree-visualize))
  :config
  (setf undo-tree-visualizer-relative-timestamps t
        undo-tree-visualizer-timestamps t))

(use-package ispell
  :ensure t
  :diminish ""
  :init
  (setenv "LANG" "en_US"))

(use-package flyspell
  :ensure t
  :diminish ""
  :hook (prog-mode . flyspell-prog-mode)
  :bind (:map flyspell-mode-map
		("C-," . nil)
		("C-." . nil)
		("C->" . flyspell-goto-next-error)
	        ("C-<" . flyspell-goto-next-error)

                ;; Return C-; to comment-dwim-line, hijacked by
                ;; flyspell-auto-correct-binding-previous-word.
                ;;
                ;; http://xahlee.org/emacs/reclaim_keybindings.html
                ("C-;" . comment-dwim-line)
                ("C-'" . flyspell-auto-correct-previous-word))
  :config
  (load-init-keybindings))

(use-package calc
  :ensure t
  :defer t
  :config
  (setf calc-display-trail nil))

;; Use C-c Left/Right to undo/redo window configuration changes.
(use-package winner
  :ensure t
  :init
  (winner-mode 1)
  :config
  (windmove-default-keybindings))

(use-package legalese
  :ensure t)

(which-function-mode)

;; display available keybindings in minibuffer popup
(use-package which-key
  :ensure t
  :diminish ""
  :init
  (which-key-mode))

(use-package volatile-highlights
  :ensure t
  :diminish "")

;; Add face for parentheses, which is used by themes to gray out the
;; parens.
(use-package paren-face
  :ensure t
  :init
  (global-paren-face-mode))

;; Offer vlf when visiting very large files.
(use-package vlf-setup
  :ensure vlf)

(use-package eldoc
  :diminish "")

(use-package subword
  :diminish "")

;; auto-revert-mode doesn't match its package name (autorevert) and
;; needs explicit handling.
(use-package autorevert
  :diminish auto-revert-mode "")

(use-package wgrep
  :ensure t)

(use-package yaml-mode
  :ensure t)

(use-package eww
  :bind (:map eww-mode-map
              ("I" . my/eww-toggle-images+fonts)
              :map eww-link-keymap
              ("I" . my/eww-toggle-images+fonts))
  :hook (eww-mode . my/shr-turn-off-images+fonts-in-buffer)
  :config
  (defun my/eww-toggle-images+fonts ()
    "Toggle whether images+fonts are loaded, and reload the current page
from cache."
    (interactive)
    (setq-local shr-inhibit-images (not shr-inhibit-images))
    (setq-local shr-use-fonts (not shr-use-fonts))
    (eww-reload t)
    (message "Images and Fonts are now %s."
             (if shr-inhibit-images "off" "on"))))

(use-package ace-link
  :ensure t
  :config
  ;; The default key is "o" (open). Use "f" (follow). "l" (last) takes
  ;; one back to the previous page.
  (ace-link-setup-default "f"))

(quelpa
 '(youtube-dl
   :fetcher github
   :repo "skeeto/youtube-dl-emacs"
   :commit "af877b5bc4f01c04fccfa7d47a2c328926f20ef4")) ; 2018-10-12

(use-package youtube-dl
  :if (my/own-system-p)
  :ensure nil          ; not in package-archives; installed by quelpa.
  :defer t
  :bind ("C-x y" . youtube-dl-list)
  :config
  (setq youtube-dl-directory "~/videos"))

(use-package edit-indirect
  :ensure t)

(use-package htmlize
  :ensure t)

(use-package abbrev
  :diminish "")

(use-package bat-mode
  :ensure nil
  :init
  :hook (bat-mode . (lambda()
                      (setq-local comment-start "@rem "
                                  comment-start-skip "@?rem[ \t]+"))))

(provide 'init-misc)
