;; The "+" variant  of the "exec" command executes "ls" on the whole
;; file-list while the the default ";" variant executes "ls" for each
;; file found (much slower). This is better than
;;
;; (setq find-ls-option '("-print0 | xargs -0 ls -ld" . "-ld"))
;;
;; since the latter spawns a new process (xargs) while the former uses
;; an option (exec) from the current process (find)).
(use-package find-dired
  :ensure t
  :config
  (setq find-ls-option '("-exec ls -ld {} \\+" . "-ld")))

(use-package dired-rsync
  :ensure t
  :config
  (setq
   dired-rsync-options "-avzP"
   dired-rsync-unmark-on-completion nil))

(use-package dired
  :config
  (put 'dired-find-alternate-file 'disabled nil)
  (setf
   ;; http://oremacs.com/2015/01/13/dired-options/
   dired-listing-switches "-lAGh1v --group-directories-first"

   dired-tex-unclean-extensions '(".toc" ".log" ".aux" ".out" ".nav"
                                  ".snm")

   directory-free-space-args "-Pkh"
   list-directory-verbose-switches "-al"
   dired-dwim-target t
   dired-omit-mode nil
   dired-recursive-copies 'always
   dired-recursive-deletes 'always)

  ;; Instead of taking me to the very beginning or very end in Dired,
  ;; take me to the first or last file.
  (defun dired-back-to-top ()
    (interactive)
    (beginning-of-buffer)
    (dired-next-line 1))

  (defun dired-jump-to-bottom ()
    (interactive)
    (end-of-buffer)
    (dired-next-line -1))

  ;; http://oremacs.com/2015/01/12/dired-file-size/
  (defun dired-get-size ()
    (interactive)
    (let ((files (dired-get-marked-files)))
      (with-temp-buffer
        (apply 'call-process "/usr/bin/du" nil t nil "-sch" files)
        (message
         "Size of all marked items: %s"
         (progn
	   (re-search-backward "\\(^[0-9.,]+[A-Za-z]+\\).*total$")
	   (match-string 1))))))

  :hook (dired-mode . (lambda ()
                        (turn-on-gnus-dired-mode)
                        (dired-hide-details-mode)
                        (dired-sort-toggle-or-edit)))

  :bind (:map dired-mode-map
              ([remap beginning-of-buffer] . dired-back-to-top)
              ([remap end-of-buffer]       . dired-jump-to-bottom)
              ("C-z" . dired-get-size)
              ("Y"   . dired-rsync))
  ) ; end of (use-package dired)

(use-package openwith
  :ensure t
  :config
  (setq
   openwith-confirm-invocation t
   openwith-associations '(("\\.\\(pdf\\|jpeg\\|jpg\\|png\\|mp3\\|ogg\\|ods\\|xlsx\\|docx\\|mp4\\|webm\\|avi\\|mov\\|mkv\\)$"
           "xdg-open" (file))))
  (openwith-mode t))

(provide 'init-dired)
