(when (eq system-type 'windows-nt)
  (set-face-attribute 'default nil :height 130)

  ;; Since we are providing only the executable name here, make sure
  ;; the folder it resides in (e.g., C:\opt\hunspell-1.3.2-3-w32\bin)
  ;; is on Windows PATH user environment variable.
  (setq ispell-program-name "hunspell"))

(provide 'init-os-specific)
