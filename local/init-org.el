(use-package org
  :init
  :hook (org-mode . load-init-keybindings)
  :hook (org-mode . (lambda()
                      (local-set-key (kbd "<f5>")
                                     (kbd "C-x C-s C-c C-e l O"))))
  :hook (org-mode . (lambda()
                      ;; Open PDFs visited in Org-mode in Evince (and
                      ;; not in the default choice).
                      ;;
                      ;; https://stackoverflow.com/a/8836108
                      (delete '("\\.pdf\\'" . default) org-file-apps)
                      (add-to-list
                       'org-file-apps '("\\.pdf\\'" . "evince %s"))))


  ;; Map "C-tab" to the function "other-window" in org mode, just as
  ;; in other modes.
  :hook (org-mode . (lambda ()
                     (local-set-key [(control tab)] 'ace-window)))


  :config
  ;; The default value of org-link-file-path-type is adaptive, which
  ;; means make relative links if the link is to the current directory
  ;; below, otherwise fully qualify the link. You can force it to always
  ;; be relative using the following, after which you can still
  ;; fully-qualify links on a link-by-link basis by passing one
  ;; universal argument C-u.
  ;;
  ;; https://emacs.stackexchange.com/a/16734
  (setf org-link-file-path-type 'relative
        org-return-follows-link nil
        org-highlight-latex-and-related '(latex script entities)
        org-src-fontify-natively t
        org-log-done t
        org-confirm-babel-evaluate nil)

  ;; http://orgmode.org/manual/Languages.html#Languages
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp .	t)
     (shell	 .	t)
     (python	 .	t)
     (octave	 .	t)
     (C		 .	t)
     (fortran	 .	t)
     (R		 .	t)
     (calc	 .	t)
     ))

  ;; Give some hint when running a code block.
  ;; https://stackoverflow.com/a/25247986
  (defadvice org-babel-execute-src-block (around progress nil activate)
    (message "Running code block")
    ad-do-it
    (message "Done with code block"))

  ;; Remove special treatment of SVG images in ox-html.el
  ;;
  ;; (if (string= "svg" (file-name-extension source))
  ;; (org-html--svg-image source attributes info)

  ;; https://emacs.stackexchange.com/questions/18106/how-to-export-svg-images-as-img-in-html-export
  ;; https://stackoverflow.com/questions/15717103/preferred-method-of-overriding-an-emacs-lisp-function
  (defun org-html--format-image (source attributes info)
    "Return \"img\" tag with given SOURCE and ATTRIBUTES.
SOURCE is a string specifying the location of the image.
ATTRIBUTES is a plist, as returned by
`org-export-read-attribute'. INFO is a plist used as a
communication channel."
    (org-html-close-tag
     "img"
     (org-html--make-attribute-string
      (org-combine-plists
       (list :src source
	     :alt (if (string-match-p "^ltxpng/" source)
		      (org-html-encode-plain-text
		       (org-find-text-property-in-string
                        'org-latex-src source))
		    (file-name-nondirectory source)))
       attributes))
     info))

  ;; Activate RefTeX in Org mode
  ;;
  ;; Enable the RefTeX citation function in Org-mode. With this setup,
  ;; C-c ) will invoke reftex-citation which will insert a reference in
  ;; the usual way.
  (defun org-mode-reftex-setup ()
    (load-library "reftex")
    (and (buffer-file-name)
         (file-exists-p (buffer-file-name))
         (reftex-parse-all))
    (define-key org-mode-map (kbd "C-c )") 'reftex-citation))
  (add-hook 'org-mode-hook 'org-mode-reftex-setup)

  (setf bibtex-align-at-equal-sign nil
        bibtex-comma-after-last-field t
        org-export-babel-evaluate 'inline-only

        bibtex-entry-format (quote
                             (opts-or-alts required-fields
                                           numerical-fields
                                           whitespace
                                           inherit-booktitle
                                           realign last-comma
                                           delimiters unify-case
                                           braces sort-fields))

        org-latex-pdf-process (list "latexmk -pdf -bibtex %f"))

  ) ; end of (use-package org

(use-package ox-jira
  :ensure t)

(provide 'init-org)
