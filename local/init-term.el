(use-package term
  :bind (:map term-mode-map
              ("C-c C-t" . term-toggle-mode)
              :map term-raw-map
              ("C-c C-t" . term-toggle-mode))
  :config
  (defun term-toggle-mode ()
    (interactive)
    (if (term-in-line-mode)
        (term-char-mode)
      (term-line-mode))))

(use-package comint
  :defer t
  :bind (:map comint-mode-map
              ("<down>" . comint-next-input)
              ("<up>"   . comint-previous-input)
              ("C-n"    . comint-next-input)
              ("C-p"    . comint-previous-input)
              ("C-r"    . comint-history-isearch-backward))
  :config
  (setf comint-prompt-read-only t
        comint-history-isearch t))

;; In order to edit a root-owned file on the local host enter
;; something like Ctrl+x Ctrl+f /sudo::<path-to-root-owned-file>. The
;; double colon seems different from other similar constructs but all
;; that's happening is that "/sudo::" is really just a shortcut for
;; sudo's default, which is "/sudo:root@localhost:". You can edit
;; remote root files with Ctrl+x Ctrl+f
;; /sudo:root@remote-host:<path-to-root-owned-file>

(use-package tramp
  :defer t
  :config
  (setf tramp-persistency-file-name
        (concat temporary-file-directory "tramp-" (user-login-name)))

  (add-to-list 'tramp-default-proxies-alist
	       '(nil "\\`root\\'" "/ssh:%h:"))
  (add-to-list 'tramp-default-proxies-alist
	       '((regexp-quote (system-name)) nil nil)))

(provide 'init-term)
