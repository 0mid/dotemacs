;; mu now (as of version 1.4) defaults to the
;; [[https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html][XDG
;; Base Directory Specification]] for the default locations for
;; various files. E.g. on Unix the mu database now lives under
;; ~/.cache/mu/ rather than ~/.mu. You can still use the old location
;; by passing --muhome=~/.mu to various mu commands, or setting (setq
;; mu4e-mu-home "~/.mu") for mu4e.

;; There's a new subcommand mu init to initialize the mu database,
;; which takes the --maildir and --my-address parameters that index
;; used to take. These parameters are persistent so index does not
;; need (or accept) them anymore. mu4e now depends on those
;; parameters.

;; init only needs to be run once or when changing these parameters.
;; That implies that you need to re-index after changing these
;; parameters.

;; If a folder (e.g., Sent) falls out of sync/stops pulling from
;; remote, remove its sync state file, and run mbsync and mu again;
;; e.g.,
;;
;; rm ~/.mail_mbsync/TAB/Sent/.mbsyncstate
;; mbsync --all
;; mu_init_TAB

(use-package mu4e
  :ensure nil

  ;; Creates autoloads for these commands and defers loading of the
  ;; module until they are used.
  :commands mu4e mu4e-in-new-frame

  ;; mu4e is part of mu, which I stow after its `make install'. This
  ;; provides mu4e in the dir below.
  :load-path "~/.local/share/emacs/site-lisp/mu4e"

  :bind (:map
         mu4e-view-mode-map
         ("f"         . mu4e-view-go-to-url)    ; default is reverse
         ("g"         . mu4e-view-fetch-url)
         ("<tab>"     . shr-next-link)
         ("<backtab>" . shr-previous-link)
         :map
         mu4e-main-mode-map
         ("q"         . my/mu4e-quit))

  ;; toggle with w in view mode
  :hook (mu4e-view-mode . visual-line-mode)
  :hook (mu4e-compose-mode . turn-off-auto-fill)

  ;; Simple HTTP Renderer (shr) takes a HTML parse tree (as provided
  ;; by libxml-parse-html-region) and renders it in the current
  ;; buffer. It does not do CSS, JavaScript or anything advanced: It's
  ;; geared towards rendering typical short snippets of HTML, like
  ;; what you'd find in HTML email and the like. When viewing an email
  ;; in mu4e, for privacy, inhibit images and do not use fonts."
  :hook (mu4e-view-mode . my/shr-turn-off-images+fonts-in-buffer)
  :hook (mu4e-compose-pre . my/mu4e-set-from-address)
  :config

  (defun my/mu4e-set-from-address ()
    "Set the account FROM which a reply/forward will be sent."

    ;; If found, use own email from the TO: or FROM: (e.g., when
    ;; replying to or forwarding own email) when replying or
    ;; forwarding.
    (let ((msg mu4e-compose-parent-message))
      (when msg ; otherwise, mu4e-error: [mu4e] Message must be non-nil
        (require 'cl)
        (setq user-mail-address
              (loop for tofrom in '(:to :from :cc :bcc)
                    do (let ((from-addr
                              (find-if (lambda (addr)
                                         (mu4e-message-contact-field-matches
                                          msg tofrom addr))
                                       (mu4e-personal-addresses))))
                         (when from-addr (return from-addr))))
              )))
    ;; otherwise (e.g., email sent to BCC or MAILING LIST), present an
    ;; ivy selection "From: " prompt, with candidates from
    ;; mu4e-personal-addresses.
    (unless user-mail-address
      (ivy-read "From: " (mu4e-personal-addresses)
                :action (lambda (candidate)
                          (setq user-mail-address candidate))))
    )

  (setq my/pmail-bridge-proc-name "pmail-bridge")
  (defun my/pmail-bridge-start ()
    "Start the Protonmail Bridge process."
    (interactive)
    (start-process my/pmail-bridge-proc-name ; NAME
                   my/pmail-bridge-proc-name ; BUFFER
                   "protonmail-bridge" "--no-window"))

  (defun my/pmail-bridge-kill ()
    "Kill (interrupt) the Protonmail Bridge process."
    (interactive)
    (let* ((buf (get-buffer my/pmail-bridge-proc-name))
           (proc (and (buffer-live-p buf)
                      (get-buffer-process buf))))
      (when proc
        (let ((delete-exited-processes t))
          ;; Send SIGINT (C-c) to process, so it can exit gracefully.
          (ignore-errors
            (signal-process proc 'SIGINT))))))

  (setq my/mu4e-initial-update-done-p nil)
  (add-hook 'mu4e-update-pre-hook
            '(lambda ()
               "Wait a few seconds before doing the initial update.
This is to ensure that the protonmail-bridge has finished
starting."
               (unless my/mu4e-initial-update-done-p
                 (setq my/mu4e-initial-update-done-p t)
                 (sleep-for 4))))

  (defun mu4e-in-new-frame ()
    "Start mu4e in new frame."
    (interactive)
    (select-frame (make-frame '((name . "mu4e"))))
    (persp-switch "mu4e")
    (my/pmail-bridge-start)
    (mu4e)
    (kill-buffer "*scratch* (mu4e)"))

  (defun my/mu4e-quit ()
    "Quit mu4e (which kills all its buffers) and delete its frame."
    (interactive)
    (mu4e-quit)
    (my/pmail-bridge-kill)
    (setq my/mu4e-initial-update-done-p nil) ; reset for next start
    (delete-frame))

  (defun my/mu4e-browse-in-eww (msg)
    "View message in EWW (Emacs Web Wowser)"
    (eww-browse-url (concat "file://"
                            (mu4e~write-body-to-html msg))))

  (append-to-list-multiple 'mu4e-bookmarks
                           '(( :name  "PDF messages"
                                      :query "mime:application/pdf"
                                      :key   ?P
                                      :hide-unread t)

                             ( :name  "Large messages (>5MB)"
                                      :query "size:5M.."
                                      :key   ?L
                                      :hide-unread t)))

  ;; The first letter of action NAME is used as a shortcut character,
  ;; hence the awkward-looking names.
  (append-to-list-multiple 'mu4e-view-actions
               '(("Bbrowse" . mu4e-action-view-in-browser)
                 ("beww"    . my/mu4e-browse-in-eww)))

  ;; As of version 1.4, mu4e no longer uses the `mu4e-maildir' and
  ;; `mu4e-user-mail-address-list' variables; instead it uses the
  ;; information it gets from mu (see the mu section above). If you
  ;; have a non-default mu4e-mu-home, make sure to set it before mu4e
  ;; starts.
  (setq mu4e-attachment-dir (expand-file-name "~/temp")
        mu4e-search-full t
        mu4e-compose-in-new-frame t
        mu4e-save-multiple-attachments-without-asking t
        ;; relative to the maildir
        mu4e-sent-folder "/Sent"
        mu4e-drafts-folder "/Drafts"
        mu4e-trash-folder "/Trash"
        mu4e-refile-folder "/Archive"

        mu4e-headers-skip-duplicates t  ; toggle with V
        mu4e-headers-show-threads nil   ; toggle with P
        mu4e-view-show-addresses t      ; toggle per name with M-RET

        ;; mu4e-html2text-command "html2text -utf8 -width 75"
        ;; mu4e-html-renderer 'w3m
        ;; mu4e-html2text-command "w3m -dump -T text/html"
        mu4e-html2text-command 'mu4e-shr2text

        mu4e-compose-format-flowed nil
        mu4e-compose-keep-self-cc t
        mu4e-headers-date-format "%x"

        ;; Run upon pressing U in the main view, or C-c C-u elsewhere
        ;; (`mu index' is also run automatically after this command).
        mu4e-get-mail-command "mbsync --all"
        mu4e-change-filenames-when-moving t ; needed for mbsync

        ;; Changes in `mu4e-update-interval' only take effect after
        ;; restarting mu4e.
        mu4e-update-interval (* 15 60)  ; in seconds

        ;; Don't save messages to Sent Messages using mu4e; let the
        ;; mail provider/IMAP take care of this. Otherwise, may end up
        ;; with duplicate messages.
        mu4e-sent-messages-behavior 'delete)

  (setq mail-user-agent 'mu4e-user-agent
        message-kill-buffer-on-exit t
        message-confirm-send t

        message-send-mail-function 'smtpmail-send-it
        smtpmail-auth-credentials "~/.authinfo.gpg"
        smtpmail-smtp-server "127.0.0.1"
        smtpmail-smtp-service 1025
        smtpmail-stream-type 'starttls
        starttls-use-gnutls t)

  (add-to-list 'mm-inhibit-file-name-handlers
               'openwith-file-handler)

  ;; Attach using Dired: mark the file(s) in dired you would like to
  ;; attach and type M-x gnus-dired-attach; you’ll be asked
  ;; whether to attach them to an existing message, or create a new
  ;; one [https://www.djcbsoftware.nl/code/mu/mu4e/Dired.html].
  (use-package gnus-dired
    :config
    ;; make the `gnus-dired-mail-buffers' function also work on
    ;; message-mode derived modes, such as mu4e-compose-mode
    (defun gnus-dired-mail-buffers ()
      "Return a list of active message buffers."
      (let (buffers)
        (save-current-buffer
          (dolist (buffer (buffer-list t))
            (set-buffer buffer)
            (when (and (derived-mode-p 'message-mode)
                       (null message-sent-message-via))
              (push (buffer-name buffer) buffers))))
        (nreverse buffers))))
  (setq gnus-dired-mail-mode 'mu4e-user-agent)

  )

(use-package gnutls
  :config
  (add-to-list 'gnutls-trustfiles
               (expand-file-name
                "~/.config/protonmail/bridge/cert.pem")))

(provide 'init-mu4e)
