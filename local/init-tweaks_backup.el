(setq backup-by-copying-when-linked t)

(setq emacs-backup-directory
      (concat user-emacs-directory
	      (convert-standard-filename "backups/")))

(setq emacs-autosave-directory
      (concat user-emacs-directory
	      (convert-standard-filename "autosaves/")))

;; Put autosave files (e.g., #foo#) in one place, /not/ scattered all over
;; the file system!
(make-directory emacs-backup-directory t)
(make-directory emacs-autosave-directory t)
(setq auto-save-file-name-transforms `(("\\(?:[^/]*/\\)*\\(.*\\)"
					,(concat
					emacs-autosave-directory "\\1")
					t)))

;; Put backup files (ie foo~) in one place too. (The
;; backup-directory-alist list contains regexp=>directory mappings;
;; filenames matching a regexp are backed up in the corresponding
;; directory. Emacs will mkdir it if necessary.)
(setq backup-directory-alist (list (cons "."
					 emacs-backup-directory)))

;; Make backups of files, even when they're in version control
(setq vc-make-backup-files t)

(provide 'init-tweaks_backup)
