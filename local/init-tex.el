(use-package tex
  :ensure auctex
  :config
  ;; Smart dollar sign ($) in LaTeX
  ;;
  ;; In LaTeX mode and org mode, insert a pair of dollars ($$)
  ;; whenever dollar key ($) is pressed.
  ;; http://tex.stackexchange.com/questions/75697/auctex-how-to-cause-math-mode-dollars-to-be-closed-automatically-with-electric
  (add-hook 'LaTeX-mode-hook  '(lambda ()
                                 (define-key LaTeX-mode-map
                                   (kbd "$") 'self-insert-command)))

  (turn-on-reftex)
  (setq reftex-plug-into-auctex t)

  ;; Use \eqref instead of \ref for referring to equations
  ;; https://www.gnu.org/software/emacs/manual/html_node/reftex/Using-_005ceqref.html
  (setq reftex-label-alist '(AMSTeX))

  ;; Toggle LaTeX Math mode. This is a minor mode rebinding the key
  ;; LaTeX-math-abbrev-prefix to allow easy typing of mathematical
  ;; symbols. ` will read a character from the keyboard, and insert
  ;; the symbol as specified in LaTeX-math-default and
  ;; LaTeX-math-list. For example, `a will insert \alpha in a math
  ;; environment and \alpha{} in a text environment. If given a prefix
  ;; argument, the symbol will be surrounded by dollar signs.
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

  ;; AUCTeX is able to format commented parts of your code just as any
  ;; other part. This means LaTeX environments and TeX groups in
  ;; comments will be indented syntactically correct if the variable
  ;; LaTeX-syntactic-comments is set to t. If you disable it, comments
  ;; will be filled like normal text and no syntactic indentation will
  ;; be done.
  (setq LaTeX-syntactic-comments t)

  ;; Make AUCTeX aware of style files and multi-file documents right away
  (setq TeX-auto-save t
        TeX-parse-self t
        TeX-electric-sub-and-superscript t)
  ;; (setq-default TeX-master nil)

  (defun init-tex-mode-hook ()
    (local-set-key (kbd "<f5>")   #'run-latexmk)

    ;; Adding comment to the list of verbatim enviroments helps solve
    ;; the font lock problems in, say, org mode tables with formulas.
    (add-to-list 'LaTeX-verbatim-environments-local "comment")
    (setq TeX-PDF-mode t)

    ;; (C-c C-o C-b) hides all foldable items in the current buffer
    ;; according to the setting of TeX-fold-type-list. If you want to
    ;; have this done automatically every time you open a file, add it
    ;; to a hook and make sure the function is called after font
    ;; locking is set up for the buffer. Setting TeX-fold-mode to
    ;; non-nil accomplishes this
    (TeX-fold-mode 1))

  ;; https://stackoverflow.com/questions/7587287/how-do-i-bind-latexmk-to-one-key-in-emacs-and-have-it-show-errors-if-there-are-a
  ;; https://stackoverflow.com/questions/7885853/emacs-latexmk-function-throws-me-into-an-empty-buffer
  (defun run-latexmk ()
    (interactive)
    (let ((TeX-save-query nil)
	  (TeX-process-asynchronous nil)
	  (master-file (TeX-master-file)))
      (TeX-save-document "")
      (TeX-run-TeX "latexmk"
		   (TeX-command-expand "latexmk -pdf -bibtex %t"
                                       'TeX-master-file)
                   master-file)
      (if (plist-get TeX-error-report-switches (intern master-file))
	  (TeX-next-error t)
        (minibuffer-message "latexmk done"))))

  (setq LaTeX-command-style
        '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)")))

  (add-hook 'TeX-mode-hook 'init-tex-mode-hook)
  (add-hook 'LaTeX-mode-hook 'init-tex-mode-hook)
  (add-hook 'latex-mode-hook 'init-tex-mode-hook)

  (defun init-LaTeX-math-bm (char dollar)
    "Insert \bm{CHAR}. If DOLLAR is non-nil, put $'s around it."
    (interactive "*c\nP")
    (if dollar (insert "$"))
    (insert "\\bm{" (char-to-string char) "}")
    (if dollar (insert "$")))

  (setq LaTeX-math-list (quote (
                                ("v" init-LaTeX-math-bm "" nil)
                                )))

  ;; Automatically wrap dollars around the symbol when in text mode
  ;; without the prefix argument C-u. Must come AFTER all
  ;; LaTeX-math-list defs above to take effect on user customizations.
  ;; https://tex.stackexchange.com/questions/148563/how-can-i-streamline-insertion-of-latex-math-mode-symbols-in-auctex
  (add-hook
   'LaTeX-mode-hook
   (lambda ()
     (let ((math (reverse (append LaTeX-math-list LaTeX-math-default))))
       (while math
         (let ((entry (car math))
	       value)
           (setq math (cdr math))
           (if (listp (cdr entry))
	       (setq value (nth 1 entry))
             (setq value (cdr entry)))
           (if (stringp value)
	       (fset (intern (concat "LaTeX-math-" value))
	             (list 'lambda (list 'arg) (list 'interactive "*P")
	                   (list 'LaTeX-math-insert value
		                 '(null (texmathp)))))))))))

  ;; Delete unwanted fields (e.g., keywords, abstract, file, issn),
  ;; often followed by a C-c C-q to realign the entry.
  (defvar init-bibtex-fields-ignore-list '("keywords" "abstract" "file"
                                         "issn"))
  (defun init-bibtex-clean-entry-hook ()
    (save-excursion
      (let (bounds)
        (when (looking-at bibtex-entry-maybe-empty-head)
	  (goto-char (match-end 0))
	  (while (setq bounds (bibtex-parse-field))
	    (goto-char (bibtex-start-of-field bounds))
	    (if (member (bibtex-name-in-field bounds)
		        init-bibtex-fields-ignore-list)
	        (kill-region (caar bounds) (nth 3 bounds))
	      (goto-char (bibtex-end-of-field bounds))))))))

  (add-hook 'bibtex-clean-entry-hook 'init-bibtex-clean-entry-hook)

  ;; -----------------------------------------------------------------
  ;; CDLatex mode
  ;;
  ;; CDLaTeX mode is a minor mode that is normally used in combination
  ;; with a major LaTeX mode like AUCTeX in order to speed-up insertion
  ;; of environments and math templates. Inside Org-mode, you can make
  ;; use of some of the features of CDLaTeX mode. You need to install
  ;; cdlatex.el and texmathp.el (the latter comes also with AUCTeX) from
  ;; http://www.astro.uva.nl/~dominik/Tools/cdlatex. Don't use CDLaTeX
  ;; mode itself under Org-mode, but use the light version
  ;; org-cdlatex-mode that comes as part of Org-mode (cdlatex.el must be
  ;; on the path in either case). Turn it on for the current buffer
  ;; with M-x org-cdlatex-mode, or for all Org files with
  ;;
  ;; http://orgmode.org/manual/CDLaTeX-mode.html

  ;; add 's to the list of CDLaTeX math modifiers
  (after 'cdlatex
    (add-to-list 'cdlatex-math-modify-alist '(115 "\\mathbb" nil t
						  nil nil))
    ;; OCDL (Org-mode CDLaTeX)
    (add-hook 'org-mode-hook 'turn-on-org-cdlatex))
  ;;
  ;; -----------------------------------------------------------------

  ;; Avoid spell-checking in TeX commands by making ispell parser
  ;; aware of tex.
  (setq ispell-parser 'tex)

  ;; Change variable in math environments
  ;;
  ;; This looks for $ \( \[ \begin{equation} \begin{align} as
  ;; math-start. Other environments can be added.
  ;;
  ;; It only changes a variable, e.g., 'i', where it appears as a
  ;; complete word, not, e.g., in aib. If you want the 'i' in 'aib' to
  ;; change as well change the t in the code to nil to make a
  ;; non-delimited match.
  ;; https://tex.stackexchange.com/questions/65295/editor-that-replaces-only-text-inside-math-environments
  (defun change-mathvar (a b)
    (beginning-of-buffer)
    (interactive "sfrom: \nsto: ")
    (while (re-search-forward
	    "\\(\\\\(\\|\\\\\\[\\|[^\\\\]\$\$?\\|\\\\begin{equation}\\|\\\\begin{align}\\)" nil 1)
      (query-replace-regexp a  b t  (point)
			    (progn (re-search-forward
				    "\\(\\\\)\\|\\\\\\]\\|[^\\\\]\$\$?\\|\\\\end{equation}\\|\\\\end{align}\\)" nil 1) (point)))))

  ) ; end of (use-package tex ...

(provide 'init-tex)
