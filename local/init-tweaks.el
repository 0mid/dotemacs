;;; init-tweaks.el --- tweak basic Emacs' behavior to my liking

;;; Code:

(require 'init-tweaks_backup)
(require 'init-tweaks_clipboard)

(add-to-list 'initial-frame-alist '(fullscreen . maximized))
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(setf inhibit-startup-message t
      initial-scratch-message nil
      wdired-allow-to-change-permissions t
      echo-keystrokes 0.1
      ;; (delete-selection-mode t) ;; Overwrite marked region
      delete-active-region nil
      disabled-command-function nil
      custom-file (make-temp-file "emacs-custom")
      large-file-warning-threshold 536870911
      gc-cons-threshold (* 1024 1024 32)
      vc-display-status nil
      desktop-load-locked-desktop t
      ring-bell-function (lambda ()))

;; Use the echo area exclusively for tooltips
;; http://www.masteringemacs.org/articles/2010/10/15/making-tooltips-appear-in-echo-area/
(tooltip-mode -1)
(setq tooltip-use-echo-area t)

;; No GUIs
(menu-bar-mode -1)
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(setq use-dialog-box nil)

(blink-cursor-mode -1)

(when (fboundp 'set-horizontal-scroll-bar-mode)
  (set-horizontal-scroll-bar-mode nil))

;; no Tabs
(setq-default indent-tabs-mode nil)

;; less typing
(defalias 'yes-or-no-p 'y-or-n-p)

(prefer-coding-system 'utf-8-unix)

;; stop scrolling by huge leaps
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))
      scroll-conservatively most-positive-fixnum
      scroll-preserve-screen-position t)

(setq confirm-kill-emacs (quote yes-or-no-p))

(setq-default word-wrap t)

(when (my/own-system-p)
  (set-face-attribute 'default nil :height 110))

;; Set Emacs frame (window in other applications) title
(when window-system
  (setq frame-title-format
	'("emacs%@" (:eval (system-name)) ": "
	  (:eval (if (buffer-file-name)
		     (abbreviate-file-name (buffer-file-name)) "%b"))
	  "[%I]")))

;; Period single space ends sentence (default is period double space).
(setq sentence-end-double-space nil)

(setq enable-recursive-minibuffers t)

(use-package paren
  :config
  (show-paren-mode)
  (setf show-paren-style 'expression))

;; Transparently open compressed files
(auto-compression-mode t)

;; Don't ask for confirmation if a file or buffer does not exist when
;; you use C-x C-f or C-x b
(setq confirm-nonexistent-file-or-buffer nil)

(setq kill-ring-max 1000)

;; http://batsov.com/articles/2011/11/25/emacs-tip-number-3-whitespace-cleanup/
;; Original behavior:
;; (setq whitespace-style '(face tabs spaces trailing lines
;; 			      space-before-tab newline
;; 			      indentation empty space-after-tab
;; 			      space-mark tab-mark newline-mark)

(setq whitespace-style '(face tabs spaces trailing lines newline
			      empty space-mark tab-mark
			      newline-mark))

(add-hook 'before-save-hook 'whitespace-cleanup)

(setq skeleton-pair t)
(electric-pair-mode)

;; Indent comments according to surrounding indentation
(setq comment-style 'indent)

(put 'narrow-to-region 'disabled nil)

;; Identify 'CamelCase' as two words, 'Camel' and 'Case', just like
;; 'lisp-case' and 'snake_case' are by default identified as two words
;; each.
;;
;; http://emacsredux.com/blog/2013/04/21/camelcase-aware-editing/
(add-hook 'prog-mode-hook 'subword-mode)

;; Turn on Auto Fill mode automatically in Text mode and related modes.
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Disable auto-saving desktop due to idleness.
(setq desktop-auto-save-timeout nil)

;; Emacs 26.1 (e.g., in Debian Buster) requests the GNU Elpa repo with
;; the wrong TLS version, which makes the request fail. This is a
;; workaround for older versions of Emacs. The issue is fixed from
;; 26.3 and above upstream.
(if (string< emacs-version "26.3")
    (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

(setq gc-cons-threshold (* 100 1024 1024)) ; 100 MiB

(if (string> emacs-version "27")
    ;; This variable determines how many bytes can be read from a
    ;; sub-process in one read operation. The default, 4096 bytes, was
    ;; previously a hard-coded constant. Setting it to a larger value
    ;; might enhance throughput of reading from sub-processes that
    ;; produces vast (megabytes) amounts of data in one go (such as
    ;; lsp).
    (setq read-process-output-max (* 1 1024 1024))) ;; 1 MiB

(provide 'init-tweaks)
;;; init-tweaks.el ends here
