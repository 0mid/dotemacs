;; Align expressions at equal sign (=)
(defun align-equal ()
    (lambda ()
      (interactive)
      (replace-regexp " *= *" " = " nil (region-beginning) (region-end))
      (align-regexp (region-beginning)
                    (region-end) "\\(\\s-*\\)=" 1 1 nil)))

;; ----------------------------------------------------------------------
;; Align Matrix
;;
;; The align-regexp command allows a few options. These are activated
;; by giving a prefix to the command: C-u M-x align-regexp. The user
;; is then prompted with a few choices: First, the regular expression
;; to align with. This expression begins with \(\s-*\), which stands
;; for "an arbitrary number of spacing characters". Then the
;; parenthesis group to modify, 1 by default. This will align the
;; expression by modifying the matching whitespaces in front of the
;; regular expression, if any. The modification amounts to adding some
;; number of whitespaces to that part of the regular expression.
;; Additional number of whitespaces to add, the default 1 is fine. Set
;; to 0 if no additional whitespace is needed. Finally, answer y/n
;; depending whether the alignment must be done once or repeated.
(defun align-matrix ()
    (lambda () (interactive)
      (align-regexp (region-beginning) (region-end)
                    "\\(\\s-*\\)[[:space:]]+"
                    1 1 t)))
;;
;; ----------------------------------------------------------------------

;; It is sometimes the case that you may accidentally press Ctrl-z
;; while in Emacs and the Emacs Window magically disappears? The
;; default behavior of Ctrl-z is bound to iconify-frame which is
;; another way of saying Windows minimize. To make this more pleasant,
;; add a simple confirmation when Ctrl-z is pressed. Add the following
;; code to your .emacs file.
(defun smart-iconify-or-deiconify-frame()
  (interactive)
  (if (yes-or-no-p (format "Iconify/Deiconify Emacs? ")) (iconify-frame)))
;; (global-set-key (kbd "C-z") 'smart-iconify-or-deiconify-frame)


;; ----------------------------------------------------------------------
;; after macro
;;
;; This is simply a wrapper around eval-after-load and I have seen similar,
;; sometimes fancier, versions elsewhere but this one is mine. Normally if
;; you want something to load after a feature (package) loads you do
;; something like,
;;
;; (eval-after-load 'python
;;   (progn
;;     (message "python has been loaded")
;;     (local-set-key (kbd "M-n") 'flymake-goto-next-error)
;;     (local-set-key (kbd "M-p") 'flymake-goto-prev-error)))
;;
;; and using the after macro just cleans things up a bit,
;;
;; (after 'python
;;   (message "python has been loaded")
;;   (local-set-key (kbd "M-n") 'flymake-goto-next-error)
;;   (local-set-key (kbd "M-p") 'flymake-goto-prev-error))
;;
;; There are times you want to start Emacs but you have no packages
;; installed. Luckily when package.el installs a new package it creates an
;; autoload file that provides a feature whose name is the name of the
;; package with -autoloads appended. For example, after installing the
;; multiple-cursors package the file multiple-cursors-autoloads.el exists.
;; When package-initialize runs it loads multiple-cursors-autoloads.el
;; which then provides the multiple-cursors-autoloads feature.
;;
;; The implication is we can have code that runs only if the package is
;; actually installed simply by wrapping it in the after macro based on the
;; -autoloads feature created by package.el.
;;
;; The more obvious use of after---based on the more obvious application of
;; eval-after-load---is to run some code only after loading the package.
;; Commonly, variables are not available before loading the package and thus
;; trying to set them prior to loading can cause errors and break things.
(defmacro after (mode &rest body)
  "`eval-after-load' MODE evaluate BODY."
  (declare (indent defun))
  `(eval-after-load ,mode
     '(progn ,@body)))

;; Define a setq that is aware of the custom-set property of a variable.
;;
;; http://oremacs.com/2015/01/17/setting-up-ediff/
(defmacro csetq (variable value)
  `(funcall (or (get ',variable 'custom-set)
		'set-default)
	    ',variable ,value))

;; ----------------------------------------------------------------------
;; Give buffers in DocView mode (for PDFs converted to images and
;; such) the scroll-other-window functionality just like buffers in
;; other modes.
;;
;; http://lists.gnu.org/archive/html/emacs-devel/2008-11/msg01032.html

(defadvice scroll-other-window
  (around doc-view-scroll-up-or-next-page activate)
  "When next buffer is `doc-view-mode', do
`doc-view-scroll-up-or-next-page'."
  (other-window +1)
  (if (eq major-mode 'doc-view-mode)
      (let ((arg (ad-get-arg 0)))
	(if (null arg)
	    (doc-view-scroll-up-or-next-page)
	  (doc-view-next-line-or-next-page arg))
	(other-window -1))
    (other-window -1)
    ad-do-it))

(defadvice scroll-other-window-down (around
doc-view-scroll-down-or-previous-page activate)
  "When next buffer is `doc-view-mode', do
`doc-view-scroll-down-or-previous-page'."
  (other-window +1)
  (if (eq major-mode 'doc-view-mode)
      (let ((arg (ad-get-arg 0)))
	(if (null arg)
	    (doc-view-scroll-down-or-previous-page)
	  (doc-view-previous-line-or-previous-page arg))
	(other-window -1))
    (other-window -1)
    ad-do-it))

;;
;; ----------------------------------------------------------------------

(define-auto-insert
  (cons "\\.\\([Hh]\\|hh\\|hpp\\)\\'" "OK C/C++ header guard")
  '(nil
    (let* ((fname-nodir-noext (file-name-base buffer-file-name))
	   (ident (concat (upcase fname-nodir-noext) "_H_INCLUDED")))
      (concat "#ifndef " ident "\n"
	      "#define " ident
	      "\n\n\n\n#endif // " ident))
    ))

(defun init-term-paste ()
 (interactive)
 (process-send-string
  (get-buffer-process (current-buffer))
  (if string string (current-kill 0))))

(defun init-term-hook ()
  (goto-address-mode)
  (define-key term-raw-map "\C-y" 'init-term-paste))

;; http://www.wisdomandwonder.com/link/8610/lightweight-multiple-modes-for-semi-literate-programming
(defun init-narrow-to-region* (boundary-start boundary-end fun)
  "Edit the current region in a new, cloned, indirect buffer.
 `http://demonastery.org/2013/04/emacs-narrow-to-region-indirect/'
 `http://paste.lisp.org/display/135818Attribution'"

  (interactive "*r\naMode name? ")
  (let* ((boundary-start (if (< boundary-start 1) (point-min)
			   boundary-start))
	 (boundary-end (if (<= boundary-end boundary-start) (point-max)
			 boundary-end))
	 (new-name (concat
		    (buffer-name)
		    "⊃"
		    (number-to-string (line-number-at-pos boundary-start))
		    "-"
		    (number-to-string (line-number-at-pos boundary-end))))
	 (buf-name (generate-new-buffer-name new-name))
	 (fun (if (fboundp fun) fun
		'fundamental-mode)))
    (with-current-buffer (clone-indirect-buffer buf-name +1 +1)
      (narrow-to-region boundary-start boundary-end)
      (deactivate-mark)
      (goto-char (point-min))
      (funcall fun))))

;; Takes a multi-line paragraph and makes it into a single line of text
(defun unfill-paragraph ()
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil)))

(defun eval-and-replace (value)
  "Evaluate the sexp at point and replace it with its value."
  (interactive (list (eval-last-sexp nil)))
  (kill-sexp -1)
  (insert (format "%S" value)))

(defun what-face (pos)
  "Show the name of face under point."
  (interactive "d")
  (let ((face (or (get-char-property (point) 'read-face-name)
                  (get-char-property (point) 'face))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))

(defun toggle-tab-width ()
  (interactive)
  (let* ((loop [8 4 2])
         (match (or (cl-position tab-width loop) -1)))
    (setf tab-width (aref loop (mod (1+ match) (length loop))))))

(defun sudo-edit (&optional arg)
  "Edit currently visited file as root.
With a prefix ARG prompt for a file to visit. Will also prompt
for a file to visit if current buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo::"
                         (ido-read-file-name "Find file (as root): ")))
    (find-alternate-file (concat "/sudo::" buffer-file-name))))

(defun numcores ()
  "Return the number of logical processors on this system."
  (or
   ;; GNU/Linux
   (when (file-exists-p "/proc/cpuinfo")
     (with-temp-buffer
       (insert-file-contents "/proc/cpuinfo")
       (how-many "^processor[[:space:]]+:")))
   ;; Windows
   (let ((number-of-processors (getenv "NUMBER_OF_PROCESSORS")))
     (when number-of-processors
       (string-to-number number-of-processors)))
   ;; GNU/FreeBSD and macOS
   (with-temp-buffer
     (ignore-errors
       (when (zerop (call-process "sysctl" nil t nil "-n" "hw.ncpu"))
         (string-to-number (buffer-string)))))
   ;; Default
   1))

(defalias 'change-eol-dos-unix 'set-buffer-file-coding-system)

(defun hide-eol-caret-M ()
  "Hide ^M in buffers containing mixed (LF, CRLF, or CR) line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))

(defun append-to-list-multiple (list to-add)
  "Adds multiple items to LIST.
Allows for adding a sequence of items to the same list, rather
than having to call `add-to-list' multiple times. Prevents
duplicates."
  (interactive)
  (dolist (item to-add)
    (add-to-list list item t)))

(defun my/shr-turn-off-images+fonts-in-buffer ()
  (interactive)
  (setq-local shr-inhibit-images t)
  (setq-local shr-use-fonts nil))

(defun my/shr-turn-on-images+fonts-in-buffer ()
  (interactive)
  (setq-local shr-inhibit-images nil)
  (setq-local shr-use-fonts t))

(defun my/own-system-p ()
  (string= (system-name) "gnupower"))

(defun my/mpv (url &optional proc-prefix)
  "Play URL with mpv."
  (interactive)
  (start-process (concat proc-prefix "mpv") nil
                 "mpv" "--" url)
  (message "Opening %s with mpv ..." url))

(provide 'init-functions)
