(use-package octave
  :ensure t
  :mode ("\\.m\\'" . octave-mode) ; package is octave, mode is octave-mode
  :bind (:map inferior-octave-mode-map
              ([up]   . comint-previous-input)
              ([down] . comint-next-input))
  :init
  (setq octave-comment-char ?%)

  :config
  (turn-on-font-lock)

  ;; For some reason, adding the jit arg below heavily slows down
  ;; Inferior Octave, but not Octave run from terminal.
  ;;
  ;; (setq inferior-octave-startup-args '("--jit-compiler" "--no-gui"))

  (setf octave-block-offset 4
        comment-add 0
        octave-mode-startup-message nil
        octave-auto-indent t
        octave-auto-newline t
        octave-blink-matching-block t))

(provide 'init-octave)
