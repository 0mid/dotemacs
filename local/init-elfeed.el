(use-package elfeed
  :ensure t
  :defer t
  :commands elfeed elfeed-in-new-frame

  :init (setq url-queue-timeout 30)
  :bind (:map
         elfeed-show-mode-map
         ("j"     . next-line)
         ("k"     . previous-line)
         ("D"     . my/elfeed-show--youtube-dl)
         ("*"     . my/elfeed-show--toggle-star)
         ("v"     . my/elfeed-show--mpv-link-at-point)
         ("b"     . my/elfeed-browse-in-eww)
         ("B"     . elfeed-show-visit)  ; default is b
         ("f"     . my/ace-link-eww)
         :map
         elfeed-search-mode-map
         ("j"     . next-line)
         ("k"     . previous-line)
         ("t"     . my/elfeed-search--toggle-youtube)
         ("D"     . my/elfeed-search--youtube-dl)
         ("C-u D" . my/elfeed-search--youtube-dl-slow)
         ("L"     . youtube-dl-list)
         ("*"     . my/elfeed-search--toggle-star)
         ("v"     . my/elfeed-search--mpv-entry)
         ("b"     . my/elfeed-browse-in-eww)
         ("B"     . elfeed-search-browse-url) ; default is b
         ("q"     . my/elfeed-search--quit))

  :hook ((elfeed-show-mode
          elfeed-search-mode) . my/shr-turn-on-images+fonts-in-buffer)
  :hook (elfeed-show-mode . hide-eol-caret-M)

  :config
  (use-package shr
    :bind (:map
           shr-map
           ;; Without the following override, the "v" binding
           ;; (`shr-browse-url') in shr-map shadows that of mine
           ;; (`my/elfeed-show--mpv-link-at-point') in
           ;; elfeed-show.
           ("v" . my/elfeed-show--mpv-link-at-point)))

  ;; (push "--insecure" elfeed-curl-extra-arguments)
  (setf bookmark-default-file (locate-user-emacs-file "local/bookmarks"))
  (setf elfeed-search-title-max-width 90)

  (defun elfeed-in-new-frame ()
    "Start elfeed in new frame."
    (interactive)
    (select-frame (make-frame '((name . "elfeed"))))
    (persp-switch "elfeed")
    (elfeed)
    (kill-buffer "*scratch* (elfeed)"))

  (defun my/elfeed-search--quit ()
    "Save elfeed database and delete its frame."
    (interactive)
     (when (y-or-n-p "[elfeed] Are you sure you want to quit? ")
       (elfeed-db-save)
       (delete-frame)))

  (defun my/ace-link-eww (&optional external)
    (interactive "P")
    (require 'eww)
    (ace-link-eww external))

  (defun my/elfeed-browse-in-eww (&optional use-generic-p)
    "Browse current entry in EWW"
    (interactive "P")
    (let ((browse-url-browser-function #'eww-browse-url)
          (my/elfeed-browse-function
           (if (eq major-mode 'elfeed-show-mode)
               (lambda () (elfeed-show-visit))
             (lambda () elfeed-search-browse-url))))
      (apply my/elfeed-browse-function use-generic-p)))

  (defun my/elfeed-show--toggle (tag)
    "Toggle TAG on current entry."
    (interactive (list (intern (read-from-minibuffer "Tag: "))))
    (let ((entry elfeed-show-entry))
      (if (elfeed-tagged-p tag entry)
          (elfeed-show-untag tag)
        (elfeed-show-tag tag))))

  (defalias 'my/elfeed-show--toggle-star
    (lambda ()
      (interactive)
      (my/elfeed-show--toggle 'star))
    "Toggle `star' on current entry.")

  (defalias 'my/elfeed-search--toggle-star
    (elfeed-expose #'elfeed-search-toggle-all 'star))

  ;; Same star face looks good for both light and dark backgrounds; no
  ;; need to set them separately.
  (defface my/elfeed-search--face-star
    '((t :foreground "#859900"))
    "Marks star entries in Elfeed."
    :group 'elfeed)

  (push '(star my/elfeed-search--face-star) elfeed-search-face-alist)

  (defun my/elfeed-show--mpv-link-at-point ()
    "View (play) link at point with mpv."
    (interactive)
    (let ((url (or (elfeed-get-link-at-point)
                   (elfeed-get-url-at-point))))
      (when url
        (my/mpv url))))

  (defun my/elfeed-search--mpv-entry ()
    "View (play) current entry with mpv."
    (interactive)
    (let* ((entry (elfeed-search-selected :single))
           (url (elfeed-entry-link entry)))
      (my/mpv url)
      (elfeed-untag entry 'unread)
      (elfeed-search-update-entry entry)
      (unless elfeed-search-remain-on-entry (forward-line))))

  ;; Straight from Chris Wellons' feed-setup, with minor changes
  (defun my/elfeed-search--toggle-youtube ()
    "Toggle (+/-) youtube entries in search."
    (interactive)
    (cl-macrolet ((re (re rep str) `(replace-regexp-in-string
                                     ,re ,rep ,str)))
      (elfeed-search-set-filter
       (cond
        ((string-match-p "-youtube" elfeed-search-filter)
         (re " *-youtube" " +youtube" elfeed-search-filter))
        ((string-match-p "\\+youtube" elfeed-search-filter)
         (re " *\\+youtube" " -youtube" elfeed-search-filter))
        ((concat elfeed-search-filter " -youtube"))))))

  ;; `elfeed-{show,search}--' denote elfeed modes where these
  ;; functions will be used/bound.
  (defun my/elfeed-show--youtube-dl ()
    "Download the current entry with youtube-dl."
    (interactive)
    (pop-to-buffer (youtube-dl (elfeed-entry-link elfeed-show-entry))))

  (cl-defun my/elfeed-search--youtube-dl (&key slow)
    "Download the current entry with youtube-dl."
    (interactive)
    (let ((entries (elfeed-search-selected)))
      (dolist (entry entries)
        (if (null (youtube-dl (elfeed-entry-link entry)
                              :title (elfeed-entry-title entry)
                              :slow slow))
            (message "Entry is not a YouTube link!")
          (message "Downloading %s" (elfeed-entry-title entry)))
        (elfeed-untag entry 'unread)
        (elfeed-search-update-entry entry)
        (unless (use-region-p) (forward-line)))))

  (defalias 'my/elfeed-search--youtube-dl-slow
    (lambda ()
      (my/elfeed-search--youtube-dl :slow t)))

  (defvar my/youtube-feed-format
    '(("^UC" . "https://www.youtube.com/feeds/videos.xml?channel_id=%s")
      ("^PL" . "https://www.youtube.com/feeds/videos.xml?playlist_id=%s")
      (""    . "https://www.youtube.com/feeds/videos.xml?user=%s")))

  (defun my/elfeed-expand (listing)
    "Expand feed URLs depending on their tags."
    (cl-destructuring-bind (url . tags) listing
      (cond
       ((member 'youtube tags)
        (let* ((case-fold-search nil)
               (test (lambda (s r) (string-match-p r s)))
               (format (cl-assoc url my/youtube-feed-format :test test)))
          (cons (format (cdr format) url) tags)))
       (listing))))

  (defmacro elfeed-config (&rest feeds)
    "Minimizes feed listing indentation without being weird about it."
    (declare (indent 0))
    `(setf elfeed-feeds (mapcar #'my/elfeed-expand ',feeds)))


  (load "elfeed-config.el.gpg"
        t                               ; NOERROR if file not found
        nil                             ; print load start/end msgs
        t))                             ; don't try other suffixes

(provide 'init-elfeed)
