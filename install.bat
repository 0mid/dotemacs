@echo off
setlocal

set home=%USERPROFILE%
cd /d "%home%"

set randnum=%random%
if exist .emacs.d rename .emacs.d .emacs.d_%randnum%
if exist .emacs rename .emacs .emacs_%randnum%

mkdir .emacs.d
rem Windows 'symlink' requires admin! So, hardlink to workaround that
rem stupidity perpetuated by non-freedom.
mklink /h .emacs.d\init.el dotemacs\init.el
mklink /j .emacs.d\local dotemacs\local
rem N.B. Delete the junction dir, .emacs.d, without deleting what it
rem points to using 'rmdir .emacs.d' (NOT 'del .emacs.d', which will
rem delete the target dir, dotemacs!)
