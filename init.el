;; GNU Emacs configuration

;; Copyright (C) 2019 Omid

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(setq package-archives
      '(("elpa"		. "https://tromey.com/elpa/")
	("gnu"		. "https://elpa.gnu.org/packages/")
	("melpa"        . "https://melpa.org/packages/")
	("melpa-stable"	. "https://stable.melpa.org/packages/")
	))

(package-initialize)

;; Even with 'allow-unsigned instead of nil, package-install fails
;; due to EXPIRED GPG keys (that come with old-enough versions of
;; Emacs) used to sign packages. We are using httpS for
;; package-archives links, so this is not a terrible security risk.
(setq package-check-signature nil)

;; The rest of packages are installed by use-package's ':ensure t'.
(setq packages-to-install-without-use-package
      '(use-package))

(when (not package-archive-contents)
  (package-refresh-contents))

(dolist (pkg packages-to-install-without-use-package)
  (when (not (package-installed-p pkg))
    (package-install pkg)))

(use-package diminish
  :ensure t)

(use-package delight
  :ensure t)

(eval-when-compile
  (require 'use-package))
(require 'diminish) ;; if you use :diminish
(require 'bind-key) ;; if you use any :bind variant

(use-package quelpa
  :ensure t
  :init
  ;; Don't use MELPA recipes at all (using Quelpa mainly to install
  ;; packages not in MELPA).
  (setq quelpa-checkout-melpa-p nil))

;; my "packages"
(add-to-list 'load-path "~/.emacs.d/local")
(require 'init-functions)
(require 'init-tweaks)
(require 'init-keybindings)
(require 'init-c)
(require 'init-octave)
(require 'init-diff)
(require 'init-term)
(require 'init-dired)
(require 'init-tex)
(require 'init-org)
(require 'init-info)
(require 'init-os-specific)
(require 'init-misc)
(require 'init-theme)
(when (my/own-system-p)
  (require 'init-elfeed)
  (require 'init-mu4e))

(use-package exec-path-from-shell
  :if (memq window-system '(mac ns x))
  :ensure t
  :config
  (setq
   exec-path-from-shell-variables (quote
                                   ("PATH"
                                    "C_INCLUDE_PATH" "CPLUS_INCLUDE_PATH"
                                    "LIBRARY_PATH" "LD_LIBRARY_PATH"
                                    "MANPATH" "INFOPATH"
                                    "SSH_AUTH_SOCK"))
   exec-path-from-shell-check-startup-files nil

   ;; set to t and look at *Messages* to debug
   exec-path-from-shell-debug nil)
  (exec-path-from-shell-initialize))

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :config
  (setq magit-diff-refine-hunk 'all
        magit-last-seen-setup-instructions "1.4.0"
        git-commit-summary-max-length 78
        magit-diff-options "--word-diff")

  ;; Map "C-tab" to "ace-window" in magit mode, just as in other
  ;; modes. "C-tab" is hijacked by Magit 2.1 and does little more than
  ;; tab.
  :bind (:map magit-mode-map
              ([C-tab] . ace-window)))

(use-package projectile
  :ensure t
  ;; Remove mode name for projectile-mode, but show project name.
  :delight '(:eval (concat " " (projectile-project-name)))
  ;; :pin melpa-stable
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :config
  (setq projectile-completion-system 'ivy)
  (projectile-mode +1))

(use-package ag
  :ensure t
  :config
  (setq ag-highlight-search t))

(use-package company
  :ensure t
  :diminish ""
  :hook (after-init . global-company-mode)
  :config
  (setq company-idle-delay              0.75
	company-minimum-prefix-length   4
	company-show-numbers            t
	company-tooltip-limit           8
	company-dabbrev-downcase        nil
	company-backends                '((company-etags
                                           company-capf
                                           company-dabbrev
                                           company-dabbrev-code
                                           company-files
                                           company-keywords))
        company-transformers nil)

  :bind ([backtab] . company-complete-common))

(use-package flycheck
  :ensure t
  :if (my/own-system-p)
  :config
  (setq flycheck-global-modes nil))

(use-package hydra
  :ensure t)

(use-package lsp-mode
  :ensure t
  :if (my/own-system-p)
  :commands (lsp lsp-deferred)
  :hook ((c-mode c++-mode opencl-mode)
         . lsp-deferred)

  :bind (:map lsp-mode-map
              ("C-c M-."   . lsp-find-definition))
  :config
  (yas-global-mode +1)
  (when (my/own-system-p)
    (add-hook 'python-mode-hook 'lsp-deferred))

  ;; Let clangd use half the cores.
  ;; `-background-index' requires clangd v8+!
  (setq lsp-clients-clangd-args
        `(,(format "-j=%d" (max 1 (/ (numcores) 2)))
          "-background-index" "-log=error")
        lsp-prefer-flymake nil
        lsp-enable-xref t
        lsp-headerline-breadcrumb-enable nil
        lsp-enable-on-type-formatting nil
        lsp-enable-indentation nil))

(use-package lsp-ui
  :ensure t
  :if (my/own-system-p)
  :config
  (set-face-attribute 'lsp-ui-doc-background nil
                      :background "white")

  ;; In X (but not terminal/TTY), necessary for changes to
  ;; lsp-ui-doc-background (above) to take effect in the currently
  ;; running Emacs.
  (lsp-ui-doc--delete-frame)

  (setq lsp-ui-doc-enable nil
        lsp-ui-doc-position 'at-point
        lsp-ui-doc-include-signature t

        lsp-ui-flycheck-enable nil
        lsp-ui-flycheck-list-position 'bottom
        lsp-ui-flycheck-live-reporting t

        lsp-ui-sideline-enable nil
        lsp-ui-sideline-update-mode 'line
        lsp-ui-sideline-delay 1
        lsp-ui-sideline-ignore-duplicate t

        lsp-ui-peek-enable nil
        lsp-ui-peek-list-width 60
        lsp-ui-peek-peek-height 25
        lsp-ui-peek-fontify 'on-demand))

(use-package ivy
  :ensure t
  :demand
  :commands (ivy-mode)
  :config
  (setf ivy-use-virtual-buffers t
        ivy-count-format "%d/%d "
	enable-recursive-minibuffers t
        ivy-use-selectable-prompt t
        ;; https://github.com/abo-abo/swiper/issues/1344
        ivy-sort-matches-functions-alist '((t . nil)))
  )

;; Each "perspective" has its own buffer list and window layout. Each
;; Emacs frame has a distinct list of perspectives.
(use-package perspective
  :ensure t
  :demand t
  :bind ("C-x b" . persp-ivy-switch-buffer)
  :init
  (setq persp-mode-prefix-key (kbd "C-c x"))
  :commands persp-switch
  :hook (kill-emacs . persp-state-save)
  :config
  (setq persp-state-default-file (locate-user-emacs-file
                                  (convert-standard-filename
                                   (format ".persp-state-%s"
                                           (system-name)))))
  (persp-mode)
  (when (file-exists-p persp-state-default-file)
    (persp-state-load persp-state-default-file)))

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
         ("C-r" . swiper))
  :config
  (setq swiper-action-recenter t))

(use-package smex
  :ensure t)

(use-package counsel
  :ensure t
  :bind (("M-x"     . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-x l"   . counsel-locate)
         ("M-y"     . counsel-yank-pop)
         :map minibuffer-local-map
         ("C-r"     . counsel-minibuffer-add)
         ("M-y"     . ivy-next-line)))

;; origami-show-only-node: Close everything but the folds necessary to
;; see the point. Very useful for concentrating on an area of code.
(use-package origami
  :ensure t
  ;; origami-recursively-toggle-node: Like Org-mode header collapsing,
  ;; cycle a fold between open, recursively open, closed.
  :bind ("C-\\" . origami-recursively-toggle-node)
  :hook (prog-mode . origami-mode)
  :config
  (setq origami-show-fold-header t))

(use-package prog-mode
  :config
  ;; http://emacsredux.com/blog/2013/07/24/highlight-comment-annotations/
  (defun font-lock-comment-annotations ()
    "Highlight a bunch of well known comment annotations.
     This functions should be added to the hooks of major modes
     for programming."
    (font-lock-add-keywords
     nil '(("\\<\\(FIX\\(ME\\)?\\|TODO\\|OPTIMIZE\\|HACK\\|REFACTOR\\):"
	    1 font-lock-warning-face t))))

  (remove-hook 'prog-mode 'flycheck-mode)

  :hook (prog-mode . font-lock-comment-annotations))

(use-package yasnippet
  :ensure t
  :config
  (yas-reload-all) ; just-in-time loading unless 1st opt arg is non-nil
  (setf yas-indent-line t
        yas-also-auto-indent-first-line t))

;; Jump to char, word, etc.
(use-package avy
  :ensure t
  :bind (("M-g w" . avy-goto-word-or-subword-1)
         ("M-g c" . avy-goto-char))
  :config
  (setq avy-background t
        avy-highlight-first t
        avy-single-candidate-jump nil)
  :custom-face
  (avy-lead-face   ((t (:weight bold :foreground "blue"
                                :background "NONE"))))
  (avy-lead-face-0 ((t (:weight bold :foreground "blue"
                                :background "NONE"))))
  (avy-lead-face-2 ((t (:weight bold :foreground "blue"
                                :background "NONE"))))
  )

(use-package iedit
  :ensure t
  :bind ("C-c ." . iedit-mode)
  :init
  (setq iedit-toggle-key-default nil))

(use-package etags
  :config
  (setq
  ;; Don't ask before rereading the TAGS files if they have changed
   tags-revert-without-query t

   ;; Don't warn when TAGS files are large
   large-file-warning-threshold nil

   ;; Don't add TAGS files from other directories to the current
   ;; one.
   tags-add-tables nil))

;; Use universal ctags to build the tags database for the project.
;; When you first want to build a TAGS database run 'touch TAGS' in
;; the root directory of your project.
(use-package counsel-etags
  :ensure t
  :bind (("M-." . counsel-etags-find-tag-at-point)
         ("M-s" . counsel-ag))
  :config
  (add-to-list 'counsel-etags-ignore-filenames '"TAGS")
  (add-to-list 'counsel-etags-ignore-filenames '"*.json")

  ;; How many seconds to wait before rerunning tags for auto-update
  (setq counsel-etags-update-interval 600))

(use-package xref
  :config
    :bind ("C-c M-." . xref-find-definitions))

(use-package ivy-xref
  :ensure t
  :init (if (< emacs-major-version 27)
            (setq xref-show-xrefs-function #'ivy-xref-show-xrefs)
          (setq xref-show-definitions-function #'ivy-xref-show-defs)))

(use-package rust-mode
  :ensure t)

(use-package go-mode
  :ensure t)

(server-start)
