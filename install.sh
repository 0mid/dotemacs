#!/bin/bash

basedir="$(dirname "$(readlink -f "$0")")"

# Back up current ~/.emacs.d dir or ~/.emacs file if either exits.
datetime="$(date "+%F-%T")"
[ -d ~/.emacs.d ] && mv ~/.emacs.d{,_bak-"${datetime}"}
[ -f ~/.emacs   ] && mv ~/.emacs{,_bak-"${datetime}"}

mkdir -p ~/.emacs.d
for item in init.el local; do
    ln --backup=t -vsf "${basedir}/${item}" ~/.emacs.d/
done
